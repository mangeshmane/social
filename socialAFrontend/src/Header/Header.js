import React, { useState, useEffect } from 'react';
import Bootstrap from '../../node_modules/bootstrap/dist/css/bootstrap.css';
import { Footer } from 'react-materialize';
import { Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, } from 'reactstrap';
import instaLogo from '../img/instaLogo.jpeg';
import './Header.css';
import 'font-awesome/css/font-awesome.min.css';

export default function Header(props) {
  const [text, setText] = useState('');

  function onChange(event) {
    setText(event.target.value);
  };
  //<span className="input-group-append"><i class="fa fa-search"></i></span>
  return (
    <div>
      <Navbar className="navbar1">
        <NavItem className="navItem">
          <a href="/home"><img src={instaLogo} alt="instaLogo" className="instaLogo" /></a>
        </NavItem>
        <NavItem className="navItem2">
          <input type="text" name="search" value={text} className="searchField" onChange={onChange} placeholder="&#xF002; Search" />
        </NavItem>
        <NavItem className="navItem1">
          <a href="/home"><img src={instaLogo} alt="instaLogo1" className="instaLogo" /></a>
          <a href="/home"><img src={instaLogo} alt="instaLogo2" className="instaLogo" /></a>
          <a href="/home"><img src={instaLogo} alt="instaLogo3" className="instaLogo" /></a>
        </NavItem>
      </Navbar>
      {text}
    </div>)
}


