import React, { Component } from 'react';

import { BrowserRouter as Router, Switch, Link, Route } from 'react-router-dom';
import Register from '../Register/Register';
import Login from '../Login/Login';
import Home from '../Home/Home';
import Header from '../Header/Header';
class RoutComp extends Component {
  render() {
    return <div>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/user/" component={Home} />
          <Route path="/header" component={Header}/>
        </Switch>
      </Router>

    </div>;
  }
}

export default RoutComp;
