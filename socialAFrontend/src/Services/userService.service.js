import axios from 'axios';
import Env from '../environments/environment';

export const services = {
    saveUser,
    authUser
};

function saveUser(user) {
    debugger
    return axios.post(Env.baseUrl + '/register', user, {
        headers: {
            "Content-Type": "Application/JSON",
            "Access-Control-Allow-Origin": "*"
        }
    }).then(response => {
        return response.data
    })
}

function authUser(user) {
    return axios.post(Env.baseUrl + '/login', user, {
        headers: {
            "Content-Type": "Application/JSON",
            "Access-Control-Allow-Origin": "*"
        }
    }).then(response => {
        return response.data
    })

}


