import React, { Component } from 'react';
import social_logo from '../img/social_logo.png';
import Bootstrap from '../../node_modules/bootstrap/dist/css/bootstrap.css';
import './Login.css';
import { Link } from 'react-router-dom';
import fb_icon from '../img/fb_icon.png';
import GooglePlay from '../img/GooglePlay.png';
import AppStore from '../img/AppStore.png';
import { services } from '../Services/userService.service';
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobemail: '',
      password: ''
    }
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onHandleClick = this.onHandleClick.bind(this);
  };
  onHandleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
    console.log(this.state);
  }
  onHandleClick(e) {
    e.preventDefault();
    const user = {
      mobemail: this.state.mobemail,
      password: this.state.password
    };
    services.authUser(user).then(data => {
      console.log(data);
    })
  }

  render() {
    return <center>
      <div className="right-column text-center">
        <img src={social_logo} alt="social_logo" className="social_logo1" />
        <form className="form">
          <div className="form-group1">
            <input type="text" className="form-control1" name="mobemail" onChange={this.onHandleChange} placeholder="Phone number, username or email" />
          </div>
          <div className="form-group1">
            <input type="password" className="form-control1" name="password" onChange={this.onHandleChange} placeholder="Password" />
          </div>
          <div className="form-group1">
            <button className="btn btn-primary btn-block" onClick={this.onHandleClick}>Log in</button>
          </div>
        </form>
        <div>
          <p className="or">OR</p>
        </div>
        <div>
          <button className="btn btn-Transparent btn-block"><img src={fb_icon} alt="fb-icon" className="fb_icon1" /><span style={{ color: '#385185', fontWeight: 'bold' }}>Log in with Facebook</span></button>
        </div>
        <div>
          <a href="/#"><p style={{ fontSize: '15px' }}>Forgot password?</p></a>
        </div>
      </div>
      <div className="right-log-column text-center">
        <a href="/#" className="link"><p className="box22">Don't have an account?<Link to="/Register" className="span">Sign up</Link
        ></p></a>
      </div>
      <div className="getAccount text-center">
        <p>Get an account.</p>
      </div>
      <div>
        <a href="#"><img src={AppStore} alt="appstore" className="storeImg1" /></a>
        <a href="#"><img src={GooglePlay} alt="playstore" className="storeImg2" /></a>
      </div>
    </center>;

  }
}

export default Login;
