import React, { Component } from 'react';
import './Register.css';
import bootstrap from '../../node_modules/bootstrap/dist/css/bootstrap.css';
import social from '../img/social.jpeg';
import social_logo from '../img/social_logo.png';
import fb_icon from '../img/fb_icon.png';
import axios from 'axios';
import Env from '../environments/environment';
import { services } from '../../src/Services/userService.service';
import ReactTooltip from 'react-tooltip';


class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobemail: '',
      fname: '',
      uname: '',
      password: '',
      submit: true,
      fValid: false,
      emailValidState: false,
      passValid: false,
      passValidState: false,
      emailvalid: false,
      mobileState: false,
      uValid: false

    };
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onHandleChange(e) {
    debugger
    // e.target.name = e.target.value;
    const { name, value } = e.target;
    if (name === "mobemail") {
      if (value === '') {
        this.setState({ emailValidState: false });
        this.setState({ emailValid: true });
        this.setState({ mobileState: false });
        this.setState({ submit: true });
      }
      // else if (!/^(\d+)[\-]+(\d+)$/.test(value)) {
      //   this.setState({ mobileState: true });
      //   this.setState({ emailValidState: false });
      // }
      else if (!/^[a-zA-Z0-9]+@+[a-zA-Z0-9]+.+[A-z]/.test(value)) {
        this.setState({ emailValidState: true });
        this.setState({ emailValid: false });
        this.setState({ mobileState: false });
        this.setState({ submit: true });
      }

      else {
        this.setState({ emailValidState: false });
        this.setState({ emailValid: false });
        this.setState({ mobileState: false });
        this.setState({ submit: true });
      }
    }
    if (name === "fname") {
      if (value === '') {
        this.setState({ fValid: true });
        this.setState({ submit: true });
      } else {
        this.setState({ fValid: false });
      }
    }
    if (name === "uname") {
      if (value === '') {
        this.setState({ uValid: true });
        this.setState({ submit: true });
      }
      else {
        this.setState({ uValid: false });

      }
    }
    if (name === "password") {
      if (value === '') {
        this.setState({ passValid: true });
        this.setState({ submit: true});
      }
      else {
        this.setState({ passValid: false });
        this.setState({ submit:false });
      }
    }
    this.setState({ [name]: value });
    console.log(this.state);
  }

  onSubmit(e) {
    debugger
    e.preventDefault();
    const { mobemail, fname, uname, password } = this.state;
   
      var user = {
        mobemail: this.state.mobemail,
        fname: this.state.fname,
        uname: this.state.uname,
        password: this.state.password
      }
      // document.write(JSON.stringify(user));
      services.saveUser(user);
      setTimeout(() => { this.props.history.push("/Login") }, 5000);
    
  };



  render() {
    return (
      < div className="container">
        < div className="row" >
          <div className="col-sm-6">
            <img src={social} alt="img" className="phone" />
          </div>
          <div className="col-sm-6">
            <div className="right-column text-center">
              <img src={social_logo} alt="logo" className="social_logo" />
              <p className="info">Sign up to see photos and videos from your friends. </p>
              <button className="btn btn-primary btn-block" ><img src={fb_icon} alt="fb_icon" className="fb_icon" />Log in with facebook</button>
              <p className="or">OR</p>
              <form>
                <div className="form-group">
                  <input type="text" name="mobemail" autoComplete="off" className="form-control1" placeholder="Mobile Number or Email" onChange={this.onHandleChange} />
                </div>
                {this.state.emailValidState &&
                  <div className="iconMob" >
                    <i class="material-icons" style={{ fontsize: "48px", color: "red" }}>error_outline</i><span className="pFont">Please enter valid email</span>
                  </div>}
                {this.state.mobileState &&
                  <div className="iconMob" >
                    <i class="material-icons" style={{ fontsize: "48px", color: "red" }}>error_outline</i><span className="pFont">Please enter valid mobile</span>
                  </div>}
                {this.state.emailValid &&
                  <div className="iconMob" >
                    <i class="material-icons" style={{ fontsize: "48px", color: "red" }}>error_outline</i><span className="pFont">Please enter your mobile or email here</span>
                  </div>}
                <div className="form-group">
                  <input type="text" name="fname" className="form-control1" placeholder="Full Name" onChange={this.onHandleChange} />
                </div>
                {this.state.fValid &&
                  <div className="iconMob" >
                    <i class="material-icons" style={{ fontsize: "48px", color: "red" }}>error_outline</i><span className="pFont">Please enter your full name</span>
                  </div>}
                <div className="form-group">
                  <input type="text" name="uname" className="form-control1" placeholder="Username" onChange={this.onHandleChange} />
                </div>
                {this.state.uValid &&
                  <div className="iconMob" >
                    <i class="material-icons" style={{ fontsize: "48px", color: "red" }}>error_outline</i><span className="pFont">Please enter your username</span>
                  </div>}
                <div className="form-group">
                  <input type="password" name="password" className="form-control1" placeholder="Password" onChange={this.onHandleChange} />
                </div>

                <div>

                  <button className="btn btn-primary btn-block" name="submit" disabled={this.state.submit} onClick={this.onSubmit} >Sign Up</button>

                </div>
                <div className="terms">
                  <p>Bye Signing Up ,you agree to our<strong> Tearms, Data Policy</strong> and <strong>Cookies Policy.</strong></p>
                </div>
              </form>
            </div>
            <div className="right2-column text-center">
              <p className="box2">Have an account?<a href="Login" style={{ color: 'skyblue', cursor: 'pointer' }}> Log in </a></p>
            </div>
          </div>

        </div >
      </div >
    )
  }
}


export default Register;
