var Env = {
  baseUrl: "http://localhost:8080",
  videoUrl: "https://www.youtube.com/embed/1nKl9hVUGkQ?autoplay=1",
  sharedUrl: "http://localhost:3000/",
  defaultPageSize: 20
}

export default Env;
 