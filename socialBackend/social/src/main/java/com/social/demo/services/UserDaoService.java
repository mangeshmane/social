package com.social.demo.services;

import java.lang.module.FindException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import com.social.demo.dao.UserDao;
import com.social.demo.model.User;

@Component
public class UserDaoService implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User save(User user) {
		BCryptPasswordEncoder password=new BCryptPasswordEncoder();
		String hasPassword=password.encode(user.getPassword());
		user.setPassword(hasPassword);
		return userDao.save(user);
	}

	@Override
	public void delete(int id) {
		userDao.deleteById(id);

	}

	@Override
	public User update(User user) {
		return null;
	}

	public List<User> getUsers() {
		return null;

	}

	@Override
	public User authUser(User user) {
		 return userDao.findByUnameAndPassword(user.getUname(), user.getPassword());
		 	
//		 if(validUser.getUname()==user.getUname()) {
//		 		String resp="user authonticated";
//		 		System.out.println(resp);
//		 	}
//		 	else {
//		 		return null;
//		 	}
//		 	return  user;
	}

}
