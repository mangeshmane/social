package com.social.demo.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.social.demo.model.User;

@Service
public interface UserService {
	public User save(User user);

	public void delete(int id);

	public List<User> getUsers();

	public User update(User user);

	public User authUser(User user);
//	public Optional<User> findByUsername(User user);
}
