package com.social.demo.secConfig;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//Modifying or overriding the default spring boot security.
@Configurable
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private Object tokenProvider;

	// This method is for overriding some configuration of the WebSecurity
		// If you want to ignore some request or request patterns then you can
		// specify that inside this method
	  @Override
	    protected void configure(HttpSecurity http) throws Exception {

	        // @formatter:off
	        http
	        .authorizeRequests()//authorize all request 
	        .anyRequest().permitAll()//permit all any requests
	        .and().httpBasic();
	        // @formatter:on
	        //disable cross site request forgery
	        http.csrf().disable();

	    }

}
