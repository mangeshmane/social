package com.social.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.social.demo.model.User;
import com.social.demo.services.UserService;

@CrossOrigin(origins = "http://localhost:3000", allowedHeaders = "*")
@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/register")
	public User saveUser(@RequestBody User user) {
		return userService.save(user);
	}

	@PostMapping("/login")
	public User loginUser(@RequestBody User user) {
		return userService.authUser(user);
	}
}
